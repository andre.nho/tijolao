#define F_CPU 8000000
#define BAUD 38400

#include <stdint.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/setbaud.h>

// #define LED_ALIVE       1
// #define DEBUGGER_ACTIVE 1

static uint8_t logo[] = {
#include "logo.inc"
};

#define SET_AS_OUTPUT(port, pin) { DDR##port |= (1<<P##port##pin); }
#define SET_AS_PULLUP(port, pin) { DDR##port &= ~(1<<P##port##pin); PORT##port |= (1<<P##port##pin); }
#define SET(port, pin, v) { if(v) PORT##port |= (1<<P##port##pin); else PORT##port &= ~(1<<P##port##pin); }

// {{{ Debugger

#ifdef DEBUGGER_ACTIVE

class Debugger {
public:
    Debugger() {
        // set speed
        UBRR0H = UBRRH_VALUE;
        UBRR0L = UBRRL_VALUE;
#if USE_2X
        UCSR0A |= _BV(U2X0);
#else
        UCSR0A &= ~(_BV(U2X0));
#endif
        UCSR0C = /*_BV(URSEL) | */ _BV(UCSZ01) | _BV(UCSZ00); // 8-bit data
        UCSR0B = _BV(RXEN0) | _BV(TXEN0);  // enable RX and TX
    }

    void print(const char* s) {
        char* r = (char*)s;
        while (*r) {
            while ( !(UCSR0A & _BV(UDRE0)) );  // wait until last byte is written
            UDR0 = *r;
            ++r;
        }
    }

    /*
uint8_t rx()  // blocking
{
    while (!(UCSRA & _BV(RXC)));
    return UDR;
}
    */
};

Debugger dbg;
#  define D(...) dbg.print(__VA_ARGS__)
#else
#  define D(...)
#endif

// }}}

// {{{ Nokia 5110

class Nokia5110 {
public: 
    Nokia5110() {
        initialize_ports();
        initialize_display();
        clear_display();
    }

    void clear_display() {
        send(Mode::Command, 0b01000000);  // y = 0
        send(Mode::Command, 0b10000000);  // x = 0

        for(int i=0; i<504; ++i)
            send(Mode::Data, 0);
    }

    void checkered_display() {
        send(Mode::Command, 0b01000000);  // y = 0
        send(Mode::Command, 0b10000000);  // x = 0

        uint8_t value = 0b10101010;
        for(int x=0; x<84; ++x) {
            for(int y=0; y<6; ++y)
                send(Mode::Data, value);
            value = ~value;
        }
    }

    void display(uint8_t data[]) {
        for (int i=0; i<504; ++i)
            send(Mode::Data, data[i]);
    }

private:
    enum class Mode { Data, Command };

    void initialize_ports()
    {
        SET_AS_OUTPUT(B,2);  // SS
        SET_AS_OUTPUT(B,3);  // MOSI
        SET_AS_OUTPUT(B,5);  // SCK
        SET_AS_OUTPUT(C,0);  // D/C
        SET_AS_OUTPUT(C,1);  // RST
        SET_AS_OUTPUT(C,2);  // SCE

        // setup SPI
        SPCR = _BV(SPE)                 // enable SPI
             | _BV(MSTR);               // the uC is master
             //| _BV(SPR1) | _BV(SPR0);   // speed = freq/128 (TODO - do more tests and decide on the speed)
    }

    void initialize_display() {
        // initialization
        SCE(1);
        RST(0);
        _delay_ms(10);
        RST(1);
        _delay_ms(70);

        // configuration
        send(Mode::Command, 0b00100001);    
        //                    ^^^^^   function set:
        //                         ^    chip is active
        //                          ^   horizontal addressing
        //                           ^  use extended instruction set
        send(Mode::Command, 0b10000000 | 60);   // V_op (set constrast)
        send(Mode::Command, 0b00000100);   // temperature coefficient = 0
        send(Mode::Command, 0b00010100);   // bias system = 3
        send(Mode::Command, 0b00100010);
        //                    ^^^^^   function set:
        //                         ^    chip is active
        //                          ^   vertical addressing
        //                           ^  use basic instruction set
        send(Mode::Command, 0b00001100);
        //                    ^^^^^   display configuration:
        //                         ^ ^  normal mode

    }

    void send(Mode mode, uint8_t data) {
        DC(mode);
        SCE(0);
        
        // send SPI data
        SPDR = data;
        while(!(SPSR & (1<<SPIF))) ;

        SCE(1);
    }

    inline void DC  (Mode m) { SET(C, 0, (m == Mode::Data) ? 1 : 0); }
    inline void RST (bool v) { SET(C, 1, v); }
    inline void SCE (bool v) { SET(C, 2, v); }
};

// }}}

// {{{ Joystick

struct Buttons {
    bool _no_0     : 1;
    bool _no_1     : 1;
    bool button_x  : 1;
    bool button_y  : 1;
    bool left      : 1;
    bool right     : 1;
    bool up        : 1;
    bool down      : 1;
};
static_assert(sizeof(Buttons) == 1, "Buttons struct must have only one byte.");

class Joystick {
public:
    Joystick() {
        SET_AS_PULLUP(D,2);
        SET_AS_PULLUP(D,3);
        SET_AS_PULLUP(D,4);
        SET_AS_PULLUP(D,5);
        SET_AS_PULLUP(D,6);
        SET_AS_PULLUP(D,7);
    }

    Buttons buttons() const {
        uint8_t b = ~PIND;
        return *reinterpret_cast<Buttons*>(&b);
    }

    bool any_key_pressed() const {
        Buttons b = buttons();
        return b.button_x || b.button_y || b.up || b.down || b.left || b.right;
    }
};

// }}}

// {{{ class Audio

class Audio {
public:
    Audio() {
        SET_AS_OUTPUT(B,1);                 // audio output (OCR1A)
        set_enabled(true);
    }

    void set_note(uint8_t octave, uint8_t note) {
        uint8_t n = (octave * 12) + note;
        ICR1 = F_CPU / static_cast<uint16_t>(8 * _notes[n]);
        OCR1A = ICR1 / 2;
    }

    void set_enabled(bool v) {
        if (v != _enabled) {
            _enabled = v;
            if (v) {
                TCCR1A = _BV(COM1A1)                // clear OC1A on compare match
                       | _BV(WGM11);                // fast PWM, with ICR1 on TOP (part 1)
                TCCR1B = _BV(CS11)                  // prescaler clk/8
                       | _BV(WGM12) | _BV(WGM13);   // fast PWM, with ICR1 on TOP (part 2)
            } else {
                TCCR1A = TCCR1B = 0;
            }
        }
    }

private:
    bool _enabled = false;

    double _notes[108] {
        16.35,  // C0
        17.32,  //  C#0/Db0 
        18.35,  // D0
        19.45,  //  D#0/Eb0 
        20.60,  // E0
        21.83,  // F0
        23.12,  //  F#0/Gb0 
        24.50,  // G0
        25.96,  //  G#0/Ab0 
        27.50,  // A0
        29.14,  //  A#0/Bb0 
        30.87,  // B0
        32.70,  // C1
        34.65,  //  C#1/Db1 
        36.71,  // D1
        38.89,  //  D#1/Eb1 
        41.20,  // E1
        43.65,  // F1
        46.25,  //  F#1/Gb1 
        49.00,  // G1
        51.91,  //  G#1/Ab1 
        55.00,  // A1
        58.27,  //  A#1/Bb1 
        61.74,  // B1
        65.41,  // C2
        69.30,  //  C#2/Db2 
        73.42,  // D2
        77.78,  //  D#2/Eb2 
        82.41,  // E2
        87.31,  // F2
        92.50,  //  F#2/Gb2 
        98.00,  // G2
        103.83,  //  G#2/Ab2 
        110.00,  // A2
        116.54,  //  A#2/Bb2 
        123.47,  // B2
        130.81,  // C3
        138.59,  //  C#3/Db3 
        146.83,  // D3
        155.56,  //  D#3/Eb3 
        164.81,  // E3
        174.61,  // F3
        185.00,  //  F#3/Gb3 
        196.00,  // G3
        207.65,  //  G#3/Ab3 
        220.00,  // A3
        233.08,  //  A#3/Bb3 
        246.94,  // B3
        261.63,  // C4
        277.18,  //  C#4/Db4 
        293.66,  // D4
        311.13,  //  D#4/Eb4 
        329.63,  // E4
        349.23,  // F4
        369.99,  //  F#4/Gb4 
        392.00,  // G4
        415.30,  //  G#4/Ab4 
        440.00,  // A4
        466.16,  //  A#4/Bb4 
        493.88,  // B4
        523.25,  // C5
        554.37,  //  C#5/Db5 
        587.33,  // D5
        622.25,  //  D#5/Eb5 
        659.25,  // E5
        698.46,  // F5
        739.99,  //  F#5/Gb5 
        783.99,  // G5
        830.61,  //  G#5/Ab5 
        880.00,  // A5
        932.33,  //  A#5/Bb5 
        987.77,  // B5
        1046.50,  // C6
        1108.73,  //  C#6/Db6 
        1174.66,  // D6
        1244.51,  //  D#6/Eb6 
        1318.51,  // E6
        1396.91,  // F6
        1479.98,  //  F#6/Gb6 
        1567.98,  // G6
        1661.22,  //  G#6/Ab6 
        1760.00,  // A6
        1864.66,  //  A#6/Bb6 
        1975.53,  // B6
        2093.00,  // C7
        2217.46,  //  C#7/Db7 
        2349.32,  // D7
        2489.02,  //  D#7/Eb7 
        2637.02,  // E7
        2793.83,  // F7
        2959.96,  //  F#7/Gb7 
        3135.96,  // G7
        3322.44,  //  G#7/Ab7 
        3520.00,  // A7
        3729.31,  //  A#7/Bb7 
        3951.07,  // B7
        4186.01,  // C8
        4434.92,  //  C#8/Db8 
        4698.63,  // D8
        4978.03,  //  D#8/Eb8 
        5274.04,  // E8
        5587.65,  // F8
        5919.91,  //  F#8/Gb8 
        6271.93,  // G8
        6644.88,  //  G#8/Ab8 
        7040.00,  // A8
        7458.62,  //  A#8/Bb8 
        7902.13,  // B8
    };
};

// }}}

// {{{ LED alive

class AliveLED {
public:
    AliveLED() {
        SET_AS_OUTPUT(B, 0);
    }

    void process() {
        if (counter == 0) {
            SET(B,0, 0);
        } else if (counter == 1) {
            SET(B,0, 1);
        } else if (counter == 99) {
            counter = -1;
        }
        ++counter;
    }

private:
    int counter = 0;
};

// }}}

AliveLED  alive;

Nokia5110 lcd;
Joystick  joystick;
Audio     audio;

int main()
{
    _delay_ms(100);

    // program timer0 for about 100 Hz
    TCCR0A |= (1 << WGM01);               // setup CTC mode
    OCR0A = 78;                           // target value
    TCCR0B |= (1 << CS02) | (1 << CS00);  // prescaler Clk/1024
    TIMSK0 |= (1 << OCIE0A);              // enable interrupt

    sei();

    D("Hello!\r\n");
    lcd.display(logo);

    audio.set_note(7, 0);
    _delay_ms(300);
    audio.set_note(7, 4);
    _delay_ms(60);
    audio.set_note(7, 5);
    _delay_ms(150);
    audio.set_note(8, 0);
    _delay_ms(800);
    audio.set_enabled(false);
    
    for(;;) {
        if (joystick.any_key_pressed())
            lcd.clear_display();
    }
}

ISR (TIMER0_COMPA_vect)
{
#ifdef LED_ALIVE
    alive.process();
#endif
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
