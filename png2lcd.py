#!/usr/bin/env python3

from PIL import Image
import sys

im = Image.open(sys.argv[1])
pix = im.load()

for y in range(0, 84):
    for x1 in range(47, -1, -8):
        v = 0
        for x2 in range(0, 8, 1):
            x = x1 - x2
            if pix[x, y] != 0:
                v |= (1 << x2)
        sys.stdout.write('0x%02X' % v + ', ')
    print()
