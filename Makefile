PROJECT    = tijolao
MC         = atmega328p
# PROGRAMMER = pi_1
PROGRAMMER = avrisp
EXTRA = -b 19200 -P /dev/ttyACM0
HFUSE      = 0xD9
EFUSE      = 0xFF

# LFUSE:
#   0xFF - internal oscillator - 8 Mhz
#   0xE2 - external oscillator >= 8 Mhz
LFUSE      = 0xE2

all: $(PROJECT).hex

$(PROJECT).elf: $(PROJECT).cc
	avr-g++ -std=c++11 -Wall -Wextra -Os -mcall-prologues -o $@ $^ -mmcu=$(MC)

$(PROJECT).hex: $(PROJECT).elf
	avr-objcopy -j .text -j .data -O ihex $^ $@

test-connection:
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA)

upload: $(PROJECT).hex
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA) -U flash:w:$^

listen:
	screen /dev/ttyUSB0 38400   # press Ctrl-A '\' to kill

fuse:
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA) -U lfuse:w:$(LFUSE):m -U hfuse:w:$(HFUSE):m -U efuse:w:$(EFUSE):m

clean:
	rm -f *.elf *.hex logo.inc

.PHONY: upload clean
